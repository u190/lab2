public class FindGrade {

    public static void main(String[] args) {
	    String grade1 = "A";
        String grade2 = "B";
        String grade3 = "C";
        String grade4 = "D";
        String grade5 = "F";
        int note = 50;
        if (note<=100 && 90<=note){
            System.out.println(" Your grade is " + grade1);
        }
        else if (note<=90 && 80<=note){
            System.out.println(" Your grade is " + grade2);
        }
        else if (note<=80 && 70<=note){
            System.out.println(" Your grade is " + grade3);
        }
        else if (note<=70 && 60<=note){
            System.out.println(" Your grade is " + grade4);
        }
        else if (note<=60 && 0<=note){
            System.out.println(" Your grade is " + grade5);
        }
        else{
            System.out.println(" It is not a valid score! ");
        }


    }
}
